from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static

from django.contrib import admin

urlpatterns = patterns(
    "",
    url(r"^admin/", include(admin.site.urls)),
    url(r"", include("blogs.urls")),
    url(r"^blogs/contact/$", "blogs.views.contacts", name="contact"),
    url(r"^blogs/login/$", "blogs.views.login_view", name="login"),
    url(r"^blogs/logout/$", "blogs.views.logout_view", name="logout"),
    url(r'^blogs/register/$', "blogs.views.register", name="register"),
    url(r'^blogs/post/$', "blogs.views.add_post", name="post"),
    url(r'^blogs/update/$', "blogs.views.update", name="update"),
    url(r'^blogs/update/post/(?P<post_pk>\d+)/$', "blogs.views.update_post", name="update_post"),
    url(r'^blogs/delete/$', "blogs.views.delete", name="delete"),
    url(r'^blogs/delete/post/(?P<post_pk>\d+)/$', "blogs.views.delete_post", name="delete_post"),
)


urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
