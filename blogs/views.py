from datetime import datetime

from django.contrib.sites.models import Site
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render, render_to_response, get_object_or_404
from django.template import RequestContext
from django.template.loader import render_to_string
from django.utils import simplejson as json

from blogs.models import FeedHit, Post, Section
from blogs.settings import ALL_SECTION_NAME
from blogs.signals import post_viewed
from blogs.forms import ContactForm, LoginForm, UserForm, UserPostForm


def contacts(request):

    sections = Section.objects.all()
    if request.method == "POST":    #if the form has been submitted
    #A form bound to the POST data
 
        form = ContactForm(request.POST)
        if form.is_valid():   #All validation pass
            #Process the data in form.cleaned__data
            subject = form.cleaned_data['subject']
            message = form.cleaned_data['message']
            sender = form.cleaned_data['sender']
            cc_myself = form.cleaned_data['cc_myself']
 
            recipients = ['herztan15@gmail.com']
            if cc_myself:
                recipients.append(sender)
 
            from django.core.mail import send_mail
 
            send_mail(subject, message, sender, recipients)
 
            return HttpResponseRedirect('/') #redirect after POST
        else:
            ContactForm(request.POST)
 
    else:
        form = ContactForm()
 
    return render_to_response(
            'blogs/blog_contact.html',
            {'form':form, "sections":sections,},
            context_instance=RequestContext(request))

def login_view(request):
    context = {}
    context['check_errors'] = False

    sections = Section.objects.all()

    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = request.POST['username']
            password = request.POST['password']
 
            user = authenticate(username=username, password=password)
 
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return HttpResponseRedirect('/')
                else:
                    context['check_errors'] = True
                    pass
            else:
                context['check_errors'] = True
                pass
 
            return HttpResponseRedirect('/')
    else:
        form = LoginForm()
 
    return render_to_response('blogs/blog_login.html', 
        { 'form': form, 'sections': sections,},
        context_instance=RequestContext(request))

def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/')

def register(request):
    context = RequestContext(request)
    sections = Section.objects.all()

    # A boolean value for telling the template whether the registration was successful.
    # Set to False initially. Code changes value to True when registration succeeds.
    registered = False

    if request.method == 'POST':
        user_form = UserForm(data=request.POST)

        if user_form.is_valid():
            user = user_form.save()

            user.set_password(user.password)
            user.save()

            # Update our variable to tell the template registration was successful.
            registered = True

        # Invalid form or forms - mistakes or something else?
        else:
            print user_form.errors

    else:
        user_form = UserForm()

    return render_to_response(
            'blogs/blog_register.html',
            {'user_form': user_form, 'registered': registered, 'sections': sections},
            context)


@login_required(login_url='/blogs/login/')
def add_post(request):
    context = RequestContext(request)
    sections = Section.objects.all()
    posted = False

    if request.method == 'POST':

        post_form = UserPostForm(request.POST)

        if post_form.is_valid():
            author = Post(author=request.user)
            post_form = UserPostForm(request.POST, instance=author)
            post_form.save()
            posted = True

        else:
            print post_form.errors

    else:
        post_form = UserPostForm()

    return render_to_response(
            'blogs/blog_add_post.html',
            {'post_form': post_form, 'posted': posted, 'sections':sections},
            context)


@login_required(login_url='/blogs/login/')
def update(request):
    
    posts = Post.objects.current()
    sections = Section.objects.all()

    return render_to_response("blogs/blog_update_list.html", {
        "posts": posts, "sections": sections,
    }, context_instance=RequestContext(request))


@login_required(login_url='/blogs/login/')
def delete(request):
    
    posts = Post.objects.current()
    sections = Section.objects.all()
    
    return render_to_response("blogs/blog_delete_list.html", {
        "posts": posts, "sections": sections,
    }, context_instance=RequestContext(request))


@login_required(login_url='/blogs/login/')
def update_post(request, post_pk):
    context = RequestContext(request)
    post = Post.objects.get(pk=post_pk)
    sections = Section.objects.all()

    updated = False

    if request.method == 'POST':

        post_form = UserPostForm(request.POST)

        if post_form.is_valid():
            post_form = UserPostForm(request.POST, instance=post)
            post = post_form.save()
            updated = True

        else:
            print post_form.errors

    else:
        if post:
            post_form = UserPostForm(instance=post)
        else:
            post_form = UserPostForm()

    return render_to_response(
            'blogs/blog_update_post.html',
            {'post_form': post_form, 'updated': updated, 'post':post, 'sections': sections},
            context)


@login_required(login_url='/blogs/login/')
def delete_post(request, post_pk):

    post = Post.objects.get(pk=post_pk)

    post.delete()

    return HttpResponseRedirect('/blogs/delete/')


@login_required(login_url='/blogs/login/')
def blog_index(request):
    
    posts = Post.objects.current()
    sections = Section.objects.all()
    
    return render_to_response("blogs/blog_list.html", {
        "posts": posts, "sections": sections,
    }, context_instance=RequestContext(request))


@login_required(login_url='/blogs/login/')
def blog_section_list(request, section_slug):
    
    section = get_object_or_404(Section, slug=section_slug)
    posts   = section.posts.all()
    sections = Section.objects.all()

    return render_to_response("blogs/blog_section_list.html", {
        "section_slug": section_slug,
        "section_name": section.name,
        "posts": posts,
        "sections": sections,
    }, context_instance=RequestContext(request))


@login_required(login_url='/blogs/login/')
def blog_post_detail(request, **kwargs):

    sections = Section.objects.all()

    if "post_pk" in kwargs:
        if request.user.is_authenticated() and request.user.is_staff:
            queryset = Post.objects.all()
            post = get_object_or_404(queryset, pk=kwargs["post_pk"])
        else:
            raise Http404()
    else:
        queryset = Post.objects.current()
        queryset = queryset.filter(
            published__year = int(kwargs["year"]),
            published__month = int(kwargs["month"]),
            published__day = int(kwargs["day"]),
        )
        post = get_object_or_404(queryset, slug=kwargs["slug"])
        post_viewed.send(sender=post, post=post, request=request)
    
    return render_to_response("blogs/blog_post.html", {
        "post": post, "sections":sections,
    }, context_instance=RequestContext(request))


@login_required(login_url='/blogs/login/')
def serialize_request(request):
    data = {
        "path": request.path,
        "META": {
            "QUERY_STRING": request.META.get("QUERY_STRING"),
            "REMOTE_ADDR": request.META.get("REMOTE_ADDR"),
        }
    }
    for key in request.META:
        if key.startswith("HTTP"):
            data["META"][key] = request.META[key]
    return json.dumps(data)


@login_required(login_url='/blogs/login/')
def blog_feed(request, section=None):
    
    section = get_object_or_404(Section, slug=section)
    posts = Post.objects.filter(section=section)
    sections = Section.objects.all()

    if section is None:
        section = Section.objects.values_list('name', flat=True).order_by('name')

    current_site = Site.objects.get_current()
    
    feed_title = "%s Blog: %s" % (current_site.name, section[0].upper() + section[1:])
    
    blog_url = "http://%s%s" % (current_site.domain, reverse("blog"))
    
    url_name, kwargs = "blog_feed", {"section": section}
    feed_url = "http://%s%s" % (current_site.domain, reverse(url_name, kwargs=kwargs))
    
    if posts:
        feed_updated = posts[0].published
    else:
        feed_updated = datetime(2009, 8, 1, 0, 0, 0)
    
    # create a feed hit
    hit = FeedHit()
    hit.request_data = serialize_request(request)
    hit.save()
    
    atom = render_to_string("blogs/atom_feed.xml", {
        "feed_id": feed_url,
        "feed_title": feed_title,
        "blog_url": blog_url,
        "feed_url": feed_url,
        "feed_updated": feed_updated,
        "entries": posts,
        "current_site": current_site,
        "sections": sections,
    })
    return HttpResponse(atom, mimetype="application/atom+xml")